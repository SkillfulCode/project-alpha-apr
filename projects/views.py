from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm

# feature 13 table specific
from tasks.models import Task


# Create your views here.
@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "projects/list.html", context)


# feature 13
@login_required
def show_project(request, id):
    projects = Project.objects.get(id=id)
    # feature 13 table specific
    tasks = Task.objects.filter(project=id)
    context = {
        "projects": projects,
        # feature 13 table specific
        "tasks": tasks,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")

    else:
        form = ProjectForm()

    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
